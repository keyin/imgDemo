from os.path import join

import numpy as np
import cv2
import os

path = os.path.abspath('.')
path = join(path, '2.jpg')


def showSize(img, size):
    x = img.shape[0]
    y = img.shape[1]

    if x > size or y > size:
        mul_x = x // size
        mul_y = y // size
        if mul_x > mul_y:
            mul = mul_x + 1
        else:
            mul = mul_y + 1
    else:
        mul = 1
    width = y // mul
    height = x // mul
    return width, height

def order_point(points):
    """
    对交点坐标进行排序
    :param points:
    :return:
    """
    points_array = np.array(points)
    x_sort = np.argsort(points_array[:, 0])
    y_sort = np.argsort(points_array[:, 1])
    left_point = points_array[x_sort[0]]
    right_point = points_array[x_sort[-1]]
    top_point = points_array[y_sort[0]]
    bottom_point = points_array[y_sort[-1]]
    return np.array([left_point, top_point, right_point, bottom_point])

def target_vertax_poin(clockwise_point):
    w1 = np.linalg.norm(clockwise_point[0]-clockwise_point[1])
    w2 = np.linalg.norm(clockwise_point[2]-clockwise_point[3])
    w = w1 if w1 > w2 else w2
    h1 = np.linalg.norm(clockwise_point[1]-clockwise_point[2])
    h2 = np.linalg.norm(clockwise_point[3]-clockwise_point[0])
    h = h1 if h1 > h2 else h2
    w = int(round(w))
    h = int(round(h))
    top_left = [0, 0]
    top_right = [w, 0]
    bottom_right = [w, h]
    bottom_left = [0, h]
    return np.array([top_left, top_right, bottom_right, bottom_left], dtype=np.float32)

def draw_line(img,lines):
    for line_points in lines:
        cv2.line(img,(line_points[0][0], line_points[0][1], line_points[0][2], line_points[0][3]), (0,255,0),2,8,0)
    cv2.imshow('lineimg', img)
    cv2.waitKey(0)

def draw_point(img,points):
    for position in points:
        cv2.circle(img,position,50,(0,255,0),-1)

    size = showSize(img, 600)
    img2 = cv2.resize(img,size)
    cv2.imshow("draw_point",img2)
    cv2.waitKey(0)

# 顺时针旋转90度
def RotateClockWise90(img):
    trans_img = cv2.transpose(img)
    new_img = cv2.flip(trans_img, 1)
    return new_img


def toushi(img):
    print(img.shape)
    w = img.shape[0]
    h = img.shape[1]
    top_lef = [0, 0]
    top_right = [w, 0]
    bottom_left = [0, h]
    bottom_right = [w, h]
    nparr = np.array([top_lef, top_right, bottom_right, bottom_left], dtype=np.float32)
    arr =[(0,0),(w,0),(0,h),(w,h)]
    # line_intersect = [[40, 200], [2000, 300], [1800,2300], [100, 2400]]
    s1 = (290,620)
    s2 = (2865,590)
    s3 = (2900,2250)
    s4 = (200,2250)
    line_intersect = np.array([s1,s2,s3,s4], dtype=np.float32)
    target_clockwise_point = target_vertax_poin(line_intersect)
    # draw_point(img,line_intersect)
    matrix = cv2.getPerspectiveTransform(line_intersect, target_clockwise_point)
    endimg = cv2.warpPerspective(img, matrix,(target_clockwise_point[2][0], target_clockwise_point[2][1]))

    draw_point(img, [s1,s2,s3,s4])

    return endimg

if __name__ == '__main__':
    img = cv2.imread(path)
    size = showSize(img, 600)

    re_img = toushi(img)
    re_img = RotateClockWise90(re_img)
    re_img = RotateClockWise90(re_img)
    cv2.imwrite('444.jpg', re_img)
    end = cv2.resize(re_img, (856,540))


    cv2.imshow('1', end)
    cv2.imwrite('3333.jpg',end)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
